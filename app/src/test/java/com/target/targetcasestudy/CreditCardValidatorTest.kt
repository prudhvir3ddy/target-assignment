package com.target.targetcasestudy

import com.target.targetcasestudy.utils.CreditCardValidator
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

/**
 * Feel free to make modifications to these unit tests! Remember, you have full technical control
 * over the project, so you can use any libraries and testing strategies that see fit.
 */
class CreditCardValidatorTest {
  @Test
  fun `is credit card number valid`() = runBlocking {
    Assert.assertTrue(
      "valid credit card number should yield true",
      CreditCardValidator.validateCreditCard("4539200963566953", TestDispatcherProvider())
    )
  }

  @Test
  fun `given credit card number less than 13 digits then number should not be valid`() =
    runBlocking {
      Assert.assertFalse(
        "valid credit card number should yield false",
        CreditCardValidator.validateCreditCard("453997674120", TestDispatcherProvider())
      )
    }

  @Test
  fun `given credit card number greater than 19 digits then number should not be valid`() =
    runBlocking {
      Assert.assertFalse(
        "valid credit card number should yield false",
        CreditCardValidator.validateCreditCard("453997674120453997674120", TestDispatcherProvider())
      )
    }
}
