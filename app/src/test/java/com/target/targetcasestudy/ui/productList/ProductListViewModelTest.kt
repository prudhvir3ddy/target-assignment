package com.target.targetcasestudy.ui.productList

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.target.targetcasestudy.FakeRepository
import com.target.targetcasestudy.TestDispatcherProvider
import com.target.targetcasestudy.domain.exception.NetworkCallFailedException
import com.target.targetcasestudy.domain.repository.TargetRepository
import com.target.targetcasestudy.ui.productList.view.ProductListViewState
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import org.junit.Assert
import org.junit.Rule
import org.junit.Test


class ProductListViewModelTest {
  @Rule
  @JvmField
  val instantExecutorRule = InstantTaskExecutorRule()

  @MockK
  lateinit var repository: TargetRepository

  private val dispatchers = TestDispatcherProvider()

  @Test
  fun `when viewModel created data viewState should be in success state`() {
    val fakeRepository = FakeRepository()
    val viewModel = ProductListViewModel(fakeRepository, dispatchers)
    viewModel.productList.observeForever {
      Assert.assertEquals(
        ProductListViewState.Success(
          FakeRepository.fakeProductList.products ?: emptyList()
        ), it
      )
    }
  }


  @Test
  fun `when viewModel created data viewState should be in error state`() {
    MockKAnnotations.init(this, relaxUnitFun = true)
    val exception = NetworkCallFailedException("")
    coEvery { repository.getProductList() }.throws(exception)
    val viewModel = ProductListViewModel(repository, dispatchers)
    viewModel.productList.observeForever {
      Assert.assertEquals(ProductListViewState.Error(exception), it)
    }
  }

}