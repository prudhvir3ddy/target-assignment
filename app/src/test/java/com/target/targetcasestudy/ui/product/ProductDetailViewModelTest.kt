package com.target.targetcasestudy.ui.product

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.target.targetcasestudy.TestDispatcherProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Rule
import org.junit.Test


class ProductDetailViewModelTest {

  @Rule
  @JvmField
  val instantExecutorRule = InstantTaskExecutorRule()

  @ExperimentalCoroutinesApi
  @Test
  fun `given a big string breaks in to chunks of give chunk size`() {
    val viewModel = ProductDetailViewModel(TestDispatcherProvider())
    viewModel.productDescriptionListWords.observeForever {
      Assert.assertEquals(listOf("t", "e", "s", "t", "i", "n", "g"), it)
    }
    viewModel.generateWords("testing", 1)
  }

}