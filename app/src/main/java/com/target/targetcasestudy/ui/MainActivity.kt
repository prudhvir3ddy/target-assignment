package com.target.targetcasestudy.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.target.targetcasestudy.R
import com.target.targetcasestudy.databinding.ActivityMainBinding
import com.target.targetcasestudy.ui.payment.PaymentDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

  private lateinit var binding: ActivityMainBinding

  private lateinit var navController: NavController

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

    val navHostFragment =
      supportFragmentManager.findFragmentById(binding.container.id) as NavHostFragment

    navController = navHostFragment.findNavController()
    val appBarConfiguration = AppBarConfiguration(navController.graph)
    binding.toolbar.setupWithNavController(navController, appBarConfiguration)

    binding.toolbar.menu.findItem(R.id.paymentDialogFragment).setOnMenuItemClickListener {
      PaymentDialogFragment().show(supportFragmentManager, "CreditCardValidation")
      true
    }
  }
}
