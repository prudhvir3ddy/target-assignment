package com.target.targetcasestudy.ui.product.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.target.targetcasestudy.R
import com.target.targetcasestudy.databinding.FragmentProductDetailBinding
import com.target.targetcasestudy.ui.product.ProductDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailFragment : Fragment(R.layout.fragment_product_detail) {

  private var _binding: FragmentProductDetailBinding? = null
  private val binding: FragmentProductDetailBinding
    get() = _binding!!

  private val args: ProductDetailFragmentArgs by navArgs()
  private val viewModel: ProductDetailViewModel by viewModels()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    _binding = FragmentProductDetailBinding.bind(view)
    binding.product = args.product

    /**
     * https://www.youtube.com/watch?v=x-FcOX6ErdI&list=PLWz5rJ2EKKc9Gq6FEnSXClhYkWAStbwlC
     */
    viewModel.generateWords(args.product?.description)
    binding.rvProductDescription.adapter = FragmentDetailDescriptionAdapter()
    viewModel.productDescriptionListWords.observe(viewLifecycleOwner) {
      (binding.rvProductDescription.adapter as FragmentDetailDescriptionAdapter).submitList(it)
    }

  }

  override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
  }

}
