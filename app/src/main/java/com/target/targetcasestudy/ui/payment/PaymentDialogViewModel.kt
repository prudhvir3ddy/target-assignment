package com.target.targetcasestudy.ui.payment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.target.targetcasestudy.utils.CreditCardValidator
import com.target.targetcasestudy.utils.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PaymentDialogViewModel @Inject constructor(
  private val dispatchers: DispatcherProvider
) : ViewModel() {

  private var job: Job? = null

  private val _isCreditCardValid = MutableLiveData<Boolean>()
  val isCreditCardValid: LiveData<Boolean> =
    Transformations.distinctUntilChanged(_isCreditCardValid)

  fun validateCreditCardNumber(creditCardNumber: String) {
    job?.cancel()
    job = CoroutineScope(dispatchers.main).launch {
      _isCreditCardValid.value =
        CreditCardValidator.validateCreditCard(creditCardNumber, dispatchers)
    }
  }

  override fun onCleared() {
    super.onCleared()
    job?.cancel()
  }
}