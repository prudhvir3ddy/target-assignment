package com.target.targetcasestudy.ui.productList.view

import com.target.targetcasestudy.data.model.Product

sealed class ProductListViewState {
  data class Success(val list: List<Product>) : ProductListViewState()
  object Loading : ProductListViewState()
  data class Error(val exception: Throwable) : ProductListViewState()
}