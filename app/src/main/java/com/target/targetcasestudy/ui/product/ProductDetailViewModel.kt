package com.target.targetcasestudy.ui.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.target.targetcasestudy.utils.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
  private val dispatchers: DispatcherProvider
) : ViewModel() {

  private val _productDescriptionListWords = MutableLiveData<List<String>>()
  val productDescriptionListWords: LiveData<List<String>> = _productDescriptionListWords


  fun generateWords(description: String?, chunk: Int = 1000) {
    viewModelScope.launch(dispatchers.main) {
      _productDescriptionListWords.value = generateWordsAsync(description, chunk)
    }
  }

  /**
   * think of this function as an algorithm to break our product description in to
   * spannable strings. i am currently not able to see any pattern in this so took 1000 length
   * this might break lines, will figure this out soon
   */
  private suspend fun generateWordsAsync(description: String?, chunk: Int): List<String> =
    withContext(dispatchers.default) {
      description?.chunked(chunk) ?: emptyList()
    }
}