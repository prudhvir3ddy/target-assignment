package com.target.targetcasestudy.ui.customviews

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet

class CircularLetterTextView(
  context: Context, attrs: AttributeSet
) : androidx.appcompat.widget.AppCompatTextView(context, attrs) {


  private var strokeWidth = 1f

  private val circlePaint = Paint().apply {
    color = Color.parseColor("#ffffff")
    flags = Paint.ANTI_ALIAS_FLAG
  }
  private val strokePaint = Paint().apply {
    color = Color.parseColor("#000000")
    flags = Paint.ANTI_ALIAS_FLAG
  }

  override fun onDraw(canvas: Canvas?) {

    val h = this.height
    val w = this.width

    val diameter = if (h > w) h else w
    val radius = diameter / 2

    this.height = diameter
    this.width = diameter

    canvas?.drawCircle(radius.toFloat(), radius.toFloat(), radius.toFloat(), strokePaint)
    canvas?.drawCircle(
      radius.toFloat(),
      radius.toFloat(),
      radius.toFloat() - strokeWidth,
      circlePaint
    )

    setTextColor(Color.RED)
    super.onDraw(canvas)
  }
}