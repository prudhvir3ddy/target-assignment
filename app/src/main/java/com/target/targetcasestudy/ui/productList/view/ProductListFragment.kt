package com.target.targetcasestudy.ui.productList.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.target.targetcasestudy.R
import com.target.targetcasestudy.data.model.Product
import com.target.targetcasestudy.databinding.FragmentProductListBinding
import com.target.targetcasestudy.ui.productList.ProductListViewModel
import com.target.targetcasestudy.utils.EspressoIdlingResource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductListFragment : Fragment(R.layout.fragment_product_list) {

  private var _binding: FragmentProductListBinding? = null
  private val binding: FragmentProductListBinding
    get() = _binding!!

  private val viewModel: ProductListViewModel by viewModels()

  private val onItemClicked = { product: Product ->
    val action =
      ProductListFragmentDirections.actionProductListFragmentToProductDetailFragment(product)
    findNavController().navigate(action)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    _binding = FragmentProductListBinding.bind(view)

    setupProductsRv()
    observeViewState()
  }

  private fun observeViewState() {
    viewModel.productList.observe(viewLifecycleOwner) {

      when (it) {
        is ProductListViewState.Error -> {
          hideProgressBar()
          Snackbar.make(binding.root, it.exception.message.toString(), Snackbar.LENGTH_SHORT)
            .show()
        }
        ProductListViewState.Loading -> {
          showProgressBar()
        }
        is ProductListViewState.Success -> {
          hideProgressBar()
          EspressoIdlingResource.increment()
          (binding.rvProductList.adapter as ProductListAdapter).submitList(it.list)
          EspressoIdlingResource.decrement()
        }
      }
    }
  }

  private fun setupProductsRv() {
    binding.rvProductList.adapter = ProductListAdapter(onItemClicked)
    binding.rvProductList.addItemDecoration(
      DividerItemDecoration(
        binding.rvProductList.context,
        LinearLayoutManager.VERTICAL
      )
    )
  }

  private fun hideProgressBar() {
    binding.progressBar.visibility = View.GONE
  }

  private fun showProgressBar() {
    binding.progressBar.visibility = View.VISIBLE
  }

  override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
  }
}
