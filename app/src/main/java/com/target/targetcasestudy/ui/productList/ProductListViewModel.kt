package com.target.targetcasestudy.ui.productList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.target.targetcasestudy.domain.DataSource
import com.target.targetcasestudy.ui.productList.view.ProductListViewState
import com.target.targetcasestudy.utils.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(
  private val repository: DataSource,
  dispatchers: DispatcherProvider
) : ViewModel() {

  private val _productList = MutableLiveData<ProductListViewState>()
  val productList: LiveData<ProductListViewState> = _productList

  private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
    _productList.value = ProductListViewState.Error(exception)
  }

  init {
    viewModelScope.launch(exceptionHandler + dispatchers.main) {
      _productList.value = ProductListViewState.Loading
      val list = repository.getProductList()?.filterNotNull() ?: emptyList()
      _productList.value = ProductListViewState.Success(list)
    }
  }

}