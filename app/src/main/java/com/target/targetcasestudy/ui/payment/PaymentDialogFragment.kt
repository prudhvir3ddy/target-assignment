package com.target.targetcasestudy.ui.payment

import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.target.targetcasestudy.R
import com.target.targetcasestudy.databinding.DialogPaymentBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Dialog that displays a minimal credit card entry form.
 *
 * Your task here is to enable the "submit" button when the credit card number is valid and
 * disable the button when the credit card number is not valid.
 *
 * You do not need to input any of your actual credit card information. See `Validators.kt` for
 * info to help you get fake credit card numbers.
 *
 * You do not need to make any changes to the layout of this screen (though you are welcome to do
 * so if you wish).
 */
@AndroidEntryPoint
class PaymentDialogFragment : DialogFragment(R.layout.dialog_payment) {

  private var _binding: DialogPaymentBinding? = null
  private val binding: DialogPaymentBinding
    get() = _binding!!

  private val viewModel: PaymentDialogViewModel by viewModels()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    _binding = DialogPaymentBinding.bind(view)

    setOnClickListeners()
    validateCreditCardNumber()
    handleSubmitButton()

  }

  private fun validateCreditCardNumber() {
    binding.cardNumber.doOnTextChanged { text, _, _, _ ->
      viewModel.validateCreditCardNumber(text.toString())
    }
  }

  private fun handleSubmitButton() {
    viewModel.isCreditCardValid.observe(viewLifecycleOwner) {
      binding.submit.isEnabled = it
    }
  }

  private fun setOnClickListeners() {
    with(binding) {
      submit.setOnClickListener { dismiss() }
      cancel.setOnClickListener { dismiss() }
    }
  }

  override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
  }
}