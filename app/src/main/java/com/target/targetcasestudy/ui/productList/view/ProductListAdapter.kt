package com.target.targetcasestudy.ui.productList.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.target.targetcasestudy.data.model.Product
import com.target.targetcasestudy.databinding.ProductListItemBinding

class ProductListAdapter(
  private val onItemClicked: (Product) -> Unit
) : ListAdapter<Product, ProductListAdapter.ProductItemViewHolder>(
  ProductDiffUtil()
) {

  class ProductItemViewHolder(private val binding: ProductListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Product, onItemClicked: (Product) -> Unit) {
      binding.product = item
      binding.root.setOnClickListener { onItemClicked(item) }
    }

    companion object {
      fun from(parent: ViewGroup): ProductItemViewHolder {
        return ProductItemViewHolder(
          ProductListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
      }
    }
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductItemViewHolder {
    return ProductItemViewHolder.from(parent)
  }

  override fun onBindViewHolder(viewHolder: ProductItemViewHolder, position: Int) {
    val item = getItem(position)
    viewHolder.bind(item, onItemClicked)
  }
}

class ProductDiffUtil : DiffUtil.ItemCallback<Product>() {
  override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean =
    oldItem.id == newItem.id

  override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean = oldItem == newItem

}
