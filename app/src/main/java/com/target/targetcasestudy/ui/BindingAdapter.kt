package com.target.targetcasestudy.ui

import android.text.SpannableString
import android.text.style.StrikethroughSpan
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import androidx.databinding.BindingAdapter
import coil.load
import com.target.targetcasestudy.R

@BindingAdapter("imageUrl")
fun imageUrl(imageView: ImageView, url: String) {
  imageView.load(url)
}

@BindingAdapter("regularPriceText", "salesPriceText")
fun priceText(textView: TextView, regularPriceText: String?, salesPriceText: String?) {
  textView.text = when {
    salesPriceText != null -> salesPriceText
    regularPriceText != null -> regularPriceText
    else -> textView.context.getString(R.string.not_in_stock)
  }
}

@BindingAdapter("regularPriceDisplay", "salesPriceDisplay")
fun regularDisplayText(textView: TextView, regularPriceText: String?, salesPriceText: String?) {
  textView.visibility = salesPriceText?.let {
    View.VISIBLE
  } ?: View.GONE

  textView.visibility = regularPriceText?.let {
    View.VISIBLE
  } ?: View.GONE

  val content =
    String.format(textView.context.getString(R.string.regular_price_template), regularPriceText)
  val spannableString = SpannableString(content)
  // this can be a problem for vernacular support, let's discuss for this
  spannableString.setSpan(StrikethroughSpan(), 5, content.length, 0)

  textView.text = spannableString

}

@BindingAdapter("asyncText")
fun asyncText(view: TextView, text: String?) {
  val params = TextViewCompat.getTextMetricsParams(view)
  (view as AppCompatTextView).setTextFuture(
    PrecomputedTextCompat.getTextFuture(text ?: "", params, null)
  )
}