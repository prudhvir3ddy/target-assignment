package com.target.targetcasestudy.ui.product.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.target.targetcasestudy.databinding.TextItemBinding

class FragmentDetailDescriptionAdapter : ListAdapter<String, TextViewHolder>(
  StringItemCallback()
) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextViewHolder {
    val binding = TextItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    return TextViewHolder(binding)
  }

  override fun onBindViewHolder(holder: TextViewHolder, position: Int) {
    holder.bind(getItem(position))
  }

}

class TextViewHolder(private val binding: TextItemBinding) : RecyclerView.ViewHolder(binding.root) {

  fun bind(text: String) {
    binding.text = text
  }
}

class StringItemCallback : DiffUtil.ItemCallback<String>() {
  override fun areItemsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem

  override fun areContentsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem
}
