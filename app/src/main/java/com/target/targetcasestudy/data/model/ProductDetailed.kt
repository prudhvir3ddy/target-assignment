package com.target.targetcasestudy.data.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductDetailed(
  @Json(name = "aisle")
  val aisle: String? = null,
  @Json(name = "description")
  val description: String? = null,
  @Json(name = "id")
  val id: Int? = null,
  @Json(name = "image_url")
  val imageUrl: String? = null,
  @Json(name = "regular_price")
  val price: Price? = null,
  @Json(name = "title")
  val title: String? = null
)