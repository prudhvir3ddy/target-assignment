package com.target.targetcasestudy.data.network

import com.target.targetcasestudy.data.model.ProductDetailed
import com.target.targetcasestudy.data.model.Products
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface TargetApiService {

  @GET(Endpoint.DEALS)
  suspend fun getProductListing(): Response<Products>

  /**
   * have not used this api call as we have data already
   */
  @GET(Endpoint.DEAL_WITH_ID)
  suspend fun getProductDetail(
    @Path(Endpoint.DEAL_WITH_ID_PATH_PARAM) id: Int
  ): Response<ProductDetailed>
}