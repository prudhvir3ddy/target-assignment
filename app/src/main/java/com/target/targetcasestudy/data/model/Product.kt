package com.target.targetcasestudy.data.model


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Product(
    @Json(name = "aisle")
    val aisle: String? = null,
    @Json(name = "description")
    val description: String? = null,
    @Json(name = "id")
    val id: Int? = null,
    @Json(name = "image_url")
    val imageUrl: String? = null,
    @Json(name = "regular_price")
    val regularPrice: Price? = null,
    @Json(name = "sale_price")
    val salePrice: Price? = null,
    @Json(name = "title")
    val title: String? = null
): Parcelable