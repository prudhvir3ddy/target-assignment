package com.target.targetcasestudy.data.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Products(
  @Json(name = "products")
  val products: List<Product>? = null
)