package com.target.targetcasestudy.data.network

object Endpoint {

  const val DEALS = "deals"
  const val DEAL_WITH_ID_PATH_PARAM = "id"
  const val DEAL_WITH_ID = "$DEALS/{$DEAL_WITH_ID_PATH_PARAM}"

}