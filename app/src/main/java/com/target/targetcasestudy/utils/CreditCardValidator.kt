package com.target.targetcasestudy.utils

import kotlinx.coroutines.withContext


/**
 * For an explanation of how to validate credit card numbers read:
 *
 * https://www.freeformatter.com/credit-card-number-generator-validator.html#fakeNumbers
 *
 * This contains a breakdown of how this algorithm should work as
 * well as a way to generate fake credit card numbers for testing
 *
 * The structure and signature of this is open to modification, however
 * it *must* include a method, field, etc that returns a [Boolean]
 * indicating if the input is valid or not
 *
 * Additional notes:
 *  * This method does not need to validate the credit card issuer
 *  * This method must validate card number length (13 - 19 digits), but does not
 *    need to validate the length based on the issuer.
 *
 * @param creditCardNumber - credit card number of (13, 19) digits
 * @return true if a credit card number is believed to be valid,
 * otherwise false
 */
object CreditCardValidator {

  suspend fun validateCreditCard(
    creditCardNumber: String,
    dispatchers: DispatcherProvider
  ): Boolean = withContext(
    dispatchers.default
  ) {
    val creditCardNumberLength = creditCardNumber.length
    if (creditCardNumberLength < 13 || creditCardNumberLength > 19) {
      return@withContext false
    }
    (creditCardNumber.sumOfDoubleEvenPlace(dispatchers) + creditCardNumber.sumOfOddPlace(dispatchers)) % 10 == 0
  }

  private suspend fun String.sumOfDoubleEvenPlace(dispatchers: DispatcherProvider): Int =
    withContext(dispatchers.default) {
      var sum = 0
      var i: Int = this@sumOfDoubleEvenPlace.length - 2
      while (i >= 0) {
        sum += ((this@sumOfDoubleEvenPlace[i].toString()).toInt() * 2).getSingleDigitOrSumOfTwoDigits(
          dispatchers
        )
        i -= 2
      }
      sum
    }

  // Return this number if it is a single digit, otherwise,
  // return the sum of the two digits
  private suspend fun Int.getSingleDigitOrSumOfTwoDigits(dispatchers: DispatcherProvider): Int =
    withContext(dispatchers.default) {
      if (this@getSingleDigitOrSumOfTwoDigits < 9)
        this@getSingleDigitOrSumOfTwoDigits
      else this@getSingleDigitOrSumOfTwoDigits / 10 + this@getSingleDigitOrSumOfTwoDigits % 10
    }

  private suspend fun String.sumOfOddPlace(dispatchers: DispatcherProvider): Int =
    withContext(dispatchers.default) {
      var sum = 0
      var i: Int = this@sumOfOddPlace.length - 1
      while (i >= 0) {
        sum += (this@sumOfOddPlace[i].toString()).toInt()
        i -= 2
      }
      sum
    }
}