package com.target.targetcasestudy.domain.exception

class NetworkCallFailedException(message: String) : Exception(message)