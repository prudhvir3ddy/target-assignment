package com.target.targetcasestudy.domain.repository

import com.target.targetcasestudy.data.model.Product
import com.target.targetcasestudy.data.network.TargetApiService
import com.target.targetcasestudy.domain.DataSource
import com.target.targetcasestudy.domain.exception.NetworkCallFailedException
import com.target.targetcasestudy.utils.Constants
import javax.inject.Inject

class TargetRepository @Inject constructor(
  private val targetApiService: TargetApiService,
) : DataSource {

  override suspend fun getProductList(): List<Product?>? {
    val response = targetApiService.getProductListing()
    if (response.isSuccessful) {
      return response.body()?.products
    } else {
      throw NetworkCallFailedException(
        response.errorBody()?.toString() ?: Constants.MESSAGE_SOMETHING_WENT_WRONG
      )
    }
  }
}