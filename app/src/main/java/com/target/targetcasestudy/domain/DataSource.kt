package com.target.targetcasestudy.domain

import com.target.targetcasestudy.data.model.Product

interface DataSource {
  suspend fun getProductList(): List<Product?>?
}
