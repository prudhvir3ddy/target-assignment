package com.target.targetcasestudy.di

import com.target.targetcasestudy.domain.DataSource
import com.target.targetcasestudy.domain.repository.TargetRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class RepoModule {

  @Binds
  abstract fun provideDataSourceRepo(repository: TargetRepository): DataSource
}
