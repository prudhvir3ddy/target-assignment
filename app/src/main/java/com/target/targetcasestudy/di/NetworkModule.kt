package com.target.targetcasestudy.di

import com.target.targetcasestudy.BuildConfig
import com.target.targetcasestudy.data.network.TargetApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {
  private const val TIMEOUT_IN_SECONDS = 15L

  @Provides
  @Named("base-url")
  fun provideTargetBaseUrl(): String = "https://api.target.com/mobile_case_study_deals/v1/"

  @Provides
  @Singleton
  fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
      .apply {
        if (BuildConfig.DEBUG) {
          addInterceptor(
            HttpLoggingInterceptor().apply {
              level = HttpLoggingInterceptor.Level.BODY
            }
          )
        }
        connectTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
        writeTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
        readTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
      }
      .build()
  }

  @Provides
  @Singleton
  fun provideRetrofit(
    okHttpClient: OkHttpClient,
    @Named("base-url") baseUrl: String
  ): Retrofit {
    return Retrofit.Builder()
      .client(okHttpClient)
      .baseUrl(baseUrl)
      .addConverterFactory(MoshiConverterFactory.create())
      .build()
  }


  @Provides
  @Singleton
  fun provideTargetApiService(retrofit: Retrofit): TargetApiService =
    retrofit.create(TargetApiService::class.java)
}