package com.target.targetcasestudy.di

import com.target.targetcasestudy.BuildConfig
import com.target.targetcasestudy.data.network.TargetApiService
import com.target.targetcasestudy.utils.MockServerDispatcher
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockWebServer
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@TestInstallIn(
  components = [SingletonComponent::class],
  replaces = [NetworkModule::class]
)
object FakeNetworkModule {

  @Provides
  @Named("base-url")
  fun provideTargetFakeBaseUrl(): String = "http://127.0.0.1:8080"

  @Provides
  @Singleton
  fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
      .apply {
        if (BuildConfig.DEBUG) {
          addInterceptor(
            HttpLoggingInterceptor().apply {
              level = HttpLoggingInterceptor.Level.BODY
            }
          )
        }
      }
      .build()
  }

  @Provides
  @Singleton
  fun provideRetrofit(
    okHttpClient: OkHttpClient,
    @Named("base-url") baseUrl: String
  ): Retrofit {
    return Retrofit.Builder()
      .client(okHttpClient)
      .baseUrl(baseUrl)
      .addConverterFactory(MoshiConverterFactory.create())
      .build()
  }


  @Provides
  @Singleton
  fun provideTargetApiService(retrofit: Retrofit): TargetApiService =
    retrofit.create(TargetApiService::class.java)

  @Provides
  @Singleton
  fun provideMockWebServer(): MockWebServer {
    return MockWebServer().apply {
      dispatcher = MockServerDispatcher().RequestDispatcher()
    }
  }
}