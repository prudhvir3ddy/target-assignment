package com.target.targetcasestudy.di

import com.target.targetcasestudy.FakeRepository
import com.target.targetcasestudy.domain.DataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn

@TestInstallIn(
  components = [SingletonComponent::class],
  replaces = [RepoModule::class]
)
@Module
abstract class FakeRepoModule {

  @Binds
  abstract fun provideDataSourceRepo(repository: FakeRepository): DataSource
}