package com.target.targetcasestudy.base

import androidx.test.espresso.IdlingRegistry
import com.jakewharton.espresso.OkHttp3IdlingResource
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import javax.inject.Inject

@HiltAndroidTest
abstract class BaseUiTestNetwork {

  @get: Rule
  val hiltRule = HiltAndroidRule(this)

  @Inject
  lateinit var okHttp: OkHttpClient

  @Inject
  lateinit var mockWebServer: MockWebServer

  @Before
  fun setup() {
    hiltRule.inject()
    mockWebServer.start(8080)
    IdlingRegistry.getInstance().register(OkHttp3IdlingResource.create("okhttp", okHttp))
  }

  @After
  fun tearDown() {
    mockWebServer.shutdown()
  }

}