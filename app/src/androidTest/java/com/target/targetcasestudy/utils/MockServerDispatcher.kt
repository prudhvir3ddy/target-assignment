package com.target.targetcasestudy.utils

import com.target.targetcasestudy.data.network.Endpoint
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest


class MockServerDispatcher {

  internal inner class RequestDispatcher : Dispatcher() {
    override fun dispatch(request: RecordedRequest): MockResponse {
      return when (request.path) {
        "/${Endpoint.DEALS}" -> MockResponse().setResponseCode(200)
          .setBody(FakeResponses.productListResponse)
        else -> MockResponse().setResponseCode(404)
      }
    }
  }
}