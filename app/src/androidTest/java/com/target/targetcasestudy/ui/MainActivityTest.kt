package com.target.targetcasestudy.ui

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.target.targetcasestudy.DataBindingIdlingResource
import com.target.targetcasestudy.R
import com.target.targetcasestudy.base.BaseUiTestNetwork
import com.target.targetcasestudy.monitorActivity
import com.target.targetcasestudy.utils.EspressoIdlingResource
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.After
import org.junit.Before
import org.junit.Test

@HiltAndroidTest
class MainActivityTest : BaseUiTestNetwork() {

  private val dataBindingIdlingResource = DataBindingIdlingResource()

  @Before
  fun registerIdlingResource() {
    IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    IdlingRegistry.getInstance().register(dataBindingIdlingResource)
  }

  @After
  fun unregisterIdlingResource() {
    IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    IdlingRegistry.getInstance().unregister(dataBindingIdlingResource)
  }

  @Test
  fun test_isActivityInView() {
    val activityScenario = ActivityScenario.launch(MainActivity::class.java)
    dataBindingIdlingResource.monitorActivity(activityScenario)
    onView(withId(R.id.root_main)).check(matches(isDisplayed()))
    activityScenario.close()
  }

}