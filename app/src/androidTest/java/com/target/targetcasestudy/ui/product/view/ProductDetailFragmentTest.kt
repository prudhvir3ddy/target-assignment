package com.target.targetcasestudy.ui.product.view

import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.target.targetcasestudy.R
import com.target.targetcasestudy.data.model.Price
import com.target.targetcasestudy.data.model.Product
import com.target.targetcasestudy.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import io.mockk.mockk
import org.junit.Rule
import org.junit.Test


@HiltAndroidTest
class ProductDetailFragmentTest {

  @get: Rule
  val hiltRule = HiltAndroidRule(this)

  @Test
  fun test_isDetailFragmentVisible() {
    val navController = mockk<NavController>()
    launchFragment(navController)
    Espresso.onView(ViewMatchers.withId(R.id.root_product_detail))
      .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
  }

  private fun launchFragment(navController: NavController?) {
    val bundle = ProductDetailFragmentArgs(
      Product("", "", 1, "", Price(), Price())
    ).toBundle()

    launchFragmentInHiltContainer<ProductDetailFragment>(bundle, R.style.AppTheme) {
      Navigation.setViewNavController(view!!, navController)
    }
  }
}