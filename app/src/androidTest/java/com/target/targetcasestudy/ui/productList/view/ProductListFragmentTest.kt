package com.target.targetcasestudy.ui.productList.view

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import com.target.targetcasestudy.R
import com.target.targetcasestudy.base.BaseUiTestNetwork
import com.target.targetcasestudy.launchFragmentInHiltContainer
import com.target.targetcasestudy.utils.EspressoIdlingResource
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.After
import org.junit.Before
import org.junit.Test

@MediumTest
@HiltAndroidTest
class ProductListFragmentTest : BaseUiTestNetwork() {

  @Before
  fun registerIdlingResource() {
    IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
  }

  @After
  fun unregisterIdlingResource() {
    IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
  }


  @Test
  fun test_isListVisible_onProductListFragmentOpened() {
    launchFragmentInHiltContainer<ProductListFragment>()
    onView(withId(R.id.rv_product_list)).check(matches(isDisplayed()))
  }

  @Test
  fun test_selectListItem_isDetailFragmentVisible() {
    launchFragmentInHiltContainer<ProductListFragment>()
    onView(withId(R.id.rv_product_list)).check(matches(isDisplayed()))
    onView(withId(R.id.rv_product_list)).perform(
      RecyclerViewActions.actionOnItemAtPosition<ProductListAdapter.ProductItemViewHolder>(
        2,
        click()
      )
    )
    onView(withId(R.id.root_product_detail)).check(matches(isDisplayed()))
  }
}