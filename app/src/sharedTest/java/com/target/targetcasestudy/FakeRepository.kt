package com.target.targetcasestudy

import com.target.targetcasestudy.data.model.Price
import com.target.targetcasestudy.data.model.Product
import com.target.targetcasestudy.data.model.Products
import com.target.targetcasestudy.domain.DataSource
import javax.inject.Inject

class FakeRepository @Inject constructor() : DataSource {
  companion object {
    val fakeProductList = Products(
      listOf(
        Product("", "", 1, "", Price(), Price(), "testing"),
        Product("", "testing", 2, "", Price(), Price()),
        Product("", "testing", 3, "", Price(), Price())
      )
    )
  }

  override suspend fun getProductList(): List<Product?>? = fakeProductList.products

}