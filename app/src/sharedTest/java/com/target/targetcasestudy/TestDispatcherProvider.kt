package com.target.targetcasestudy

import com.target.targetcasestudy.utils.DispatcherProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import javax.inject.Inject

@ExperimentalCoroutinesApi
class TestDispatcherProvider @Inject constructor() : DispatcherProvider {
  private val testCoroutineDispatcher = TestCoroutineDispatcher()
  override val main: CoroutineDispatcher
    get() = testCoroutineDispatcher
  override val io: CoroutineDispatcher
    get() = testCoroutineDispatcher
  override val default: CoroutineDispatcher
    get() = testCoroutineDispatcher
}